package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exceptions.GrowingTipsLogicException;

import java.util.Objects;

public class GrowingTips {
    private int temperatureInDegrees;
    private boolean lighting;
    private int wateringPerWeek;

    public GrowingTips() {
    }

    public GrowingTips(int temperatureInDegrees, boolean lighting,
                       int wateringPerWeek) throws GrowingTipsLogicException {
        if (checkTemperatureInDegrees(temperatureInDegrees)
                && checkWateringPerWeek(wateringPerWeek)) {
            this.temperatureInDegrees = temperatureInDegrees;
            this.lighting = lighting;
            this.wateringPerWeek = wateringPerWeek;
        } else {
            throw new GrowingTipsLogicException();
        }
    }

    public int getTemperatureInDegrees() {
        return temperatureInDegrees;
    }

    public void setTemperatureInDegrees(int temperatureInDegrees)
            throws GrowingTipsLogicException {
        if (checkTemperatureInDegrees(temperatureInDegrees)) {
            this.temperatureInDegrees = temperatureInDegrees;
        } else {
            throw new GrowingTipsLogicException("temperatureInDegrees can't " +
                    "be < 99 or > -99");
        }
    }

    public boolean isLighting() {
        return lighting;
    }

    public void setLighting(boolean lighting) {
        this.lighting = lighting;
    }

    public int getWateringPerWeek() {
        return wateringPerWeek;
    }

    public void setWateringPerWeek(int wateringPerWeek)
            throws GrowingTipsLogicException {
        if (checkWateringPerWeek(wateringPerWeek)) {
            this.wateringPerWeek = wateringPerWeek;
        } else {
            throw new GrowingTipsLogicException("wateringPerWeek can't be < 0" +
                    " or > 999");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrowingTips that = (GrowingTips) o;
        return temperatureInDegrees == that.temperatureInDegrees &&
                lighting == that.lighting &&
                wateringPerWeek == that.wateringPerWeek;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperatureInDegrees, lighting, wateringPerWeek);
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperatureInDegrees=" + temperatureInDegrees +
                ", lighting=" + lighting +
                ", wateringPerWeek=" + wateringPerWeek +
                '}';
    }

    private boolean checkTemperatureInDegrees(int temperatureInDegrees) {
        return temperatureInDegrees >= -99 && temperatureInDegrees <= 99;
    }

    private boolean checkWateringPerWeek(int wateringPerWeek) {
        return wateringPerWeek >= 0 && wateringPerWeek <= 999;
    }
}
