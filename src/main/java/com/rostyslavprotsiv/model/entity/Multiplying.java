package com.rostyslavprotsiv.model.entity;

public enum Multiplying {
    LEAF, ENGRAFTMENT, SEED
}
