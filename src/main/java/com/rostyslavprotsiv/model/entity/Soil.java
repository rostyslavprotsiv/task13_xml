package com.rostyslavprotsiv.model.entity;

public enum Soil {
    PODZOLIC, GROUND, SODPODZOLIC
}
